<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseOperationsController extends Controller {

    public function getUser()
    {
        return parent::getUser();
    }

    public function userIsAuthenticated()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER'))
        {
            throw $this->createAccessDeniedException('You\'re not logged in!');
        }

        return $this->getUser();
    }

    public function getPostObject($id)
    {
        $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);

        if (!$post)
        {
            throw $this->createNotFoundException('Object of Type Post with ID: $id not found!');
        }

        return $post;
    }

    public function getCommentObject($id)
    {
        $comment = $this->getDoctrine()->getRepository('AppBundle:Comment')->find($id);

        if (!$comment)
        {
            throw $this->createNotFoundException('Object of Type Comment with ID: $id not found!');
        }

        return $comment;
    }

    public function userOwnsPost($postid)
    {
        $this->userIsAuthenticated();

        $post = $this->getPostObject($postid);

        $user = $this->getUser();

        if ($user->getId() != $post->getUserId()->getId())
        {
            throw $this->createAccessDeniedException('You can not edit/delete this object Post with ID $postid!');
        }

        return $post;
    }

    public function userOwnsComment($commentid)
    {
        $this->userIsAuthenticated();

        $comment = $this->getCommentObject($commentid);

        $user = $this->getUser();

        if ($user->getId() != $comment->getUserId()->getId())
        {
            throw $this->createAccessDeniedException('You can not edit/delete this object Comment with ID $postid!');
        }

        return $comment;
    }
}