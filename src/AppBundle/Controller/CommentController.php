<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommentController extends BaseOperationsController
{
    public function getCommentAction(\AppBundle\Entity\Comment $comment)
    {
        return $this->render('comment/comment_item.html.twig', array(
            'comment' => $comment,
            'logged_in' => $this->get('security.authorization_checker')->isGranted('ROLE_USER')
        ));
    }

    public function listCommentsAction(\AppBundle\Entity\Post $post)
    {
        $query = $this->getDoctrine()->getManager()
            ->createQuery(
                'SELECT c FROM AppBundle:Comment c WHERE c.postID = :postid ORDER BY c.upVotes DESC, c.dateCreated DESC'
            );

        $query->setParameters(array(
            'postid' => $post->getId()
        ));

        $comments = $query->getResult();

        return $this->render('comment/comment_list.html.twig', array(
            'comments' => $comments
        ));
    }

    public function voteCommentAction(Request $request)
    {
        $this->userIsAuthenticated();

        $commentid = $request->request->get('commentid', 0);
        $votetype = $request->request->get('voteType', 0);

        $comment = $this->getDoctrine()->getManager()->getRepository('AppBundle:Comment')->find($commentid);
        $post = $this->getDoctrine()->getManager()->getRepository('AppBundle:Post')->find($comment->getPostID());

        if ($comment) {
            switch ($votetype) {
                // TODO: Alterar estrutura. Criar relacional PostVotes que relaciona que users que votaram em posts.
                // cada user so pode votar num post uma vez. Seja upvote ou downvote. Neste momento podem votar sempre N vezes.
                case 1:
                    $comment->setUpVotes($comment->getupVotes() + 1);
                    break;
                case -1:
                    $comment->setDownVotes($comment->getDownvotes() + 1);
                    break;
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $view = $this->renderView('comment/comment_item.html.twig', array(
                'comment' => $comment
            ));

            return new JsonResponse(array('item' => $view, 'success' => true));
        }
    }

    public function deleteCommentAction(Request $request)
    {
        $comment = $this->userOwnsComment($request->request->get('commentid'));

        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();
        } catch (Exception $oEx)
        {
            return new JsonResponse(array('success' => false, 'message' => $oEx->getContent()));
        }

        return new JsonResponse(array('success' => true, 'message' => "Object deleted with success!"));
    }

    public function editCommentAction(Request $request)
    {
        $comment = $this->userOwnsComment($request->request->get('commentid'));

        dump($request);

        $newCommentText = $request->request->get('commentText');

        try
        {
            $em = $this->getDoctrine()->getManager();
            $comment->setComment($newCommentText);
            $comment->setDateAltered(new \DateTime('now'));
            $em->persist($comment);
            $em->flush();
        } catch (Exception $oEx) {
            return new JsonResponse(array('success' => false, 'message' => $oEx->getContent()));
        }

        $item = $this->renderView('comment/comment_item.html.twig', array(
            'comment' => $comment
        ));

        return new JsonResponse(array('success' => true, 'item' => $item));
    }
}