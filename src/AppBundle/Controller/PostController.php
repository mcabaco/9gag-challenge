<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Post;

class PostController extends BaseOperationsController
{
    public function homepageAction()
    {
        $query = $this->getDoctrine()->getManager()->createQuery('SELECT p FROM AppBundle\Entity\Post p ORDER BY p.upVotes DESC, p.dateCreated DESC');
        $posts = $query->getResult();

        return $this->render('post/posts_list.html.twig', array(
            'posts' => $posts
        ));
    }

    public function getPostAction(\AppBundle\Entity\Post $post, $detail_item = 0)
    {
        return $this->render('post/post_item.html.twig', array(
            'post' => $post,
            'detail_item' => $detail_item
        ));
    }

    public function newPostAction(Request $request, $id)
    {
        $user = $this->userIsAuthenticated();

        if ($id)
        {
            $post = $this->userOwnsPost($id);
        }
        else {
            $post = new Post();
            $post->setUserId($user);
        }

        $form = $this->createFormBuilder($post)
            ->add('title', 'textarea', array('label' => 'Title: ', 'attr' => array('onkeyup' => 'Post.ValidateInput(this);')))
            ->add('imageFile', 'vich_image', array(
                'required'      => true,
                'allow_delete'  => true, // not mandatory, default is true
                'download_link' => true, // not mandatory, default is true
            ))
            ->add('save', 'button', array('label' => 'Create Post', 'disabled' => true, 'attr' => array('onclick' => 'Post.OpenConfirmationDialog(function() {$("form").submit()} );')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('post/new_post.html.twig', array('form' => $form->createView()));
    }

    public function votePostAction(Request $request)
    {
        $this->userIsAuthenticated();

        $postid = $request->request->get('postid', 0);
        $votetype = $request->request->get('voteType', 0);
        $detail_item = $request->request->get('detail_item', 0);

        $post = $this->getDoctrine()->getManager()->getRepository('AppBundle:Post')->find($postid);

        if ($post)
        {
            switch($votetype)
            {
                // TODO: Alterar estrutura. Criar relacional PostVotes que relaciona que users que votaram em posts.
                // cada user so pode votar num post uma vez. Seja upvote ou downvote. Neste momento podem votar sempre N vezes.
                case 1:
                    $post->setUpVotes($post->getupVotes() + 1);
                    break;
                case -1:
                    $post->setDownVotes($post->getDownvotes() + 1);
                    break;
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            $view = $this->renderView('post/post_item.html.twig', array(
                'post' => $post,
                'logged_in' => $this->get('security.authorization_checker')->isGranted('ROLE_USER'),
                'detail_item' => $detail_item
            ));

            return new JsonResponse(array('item' => $view, 'success' => true));
        }

        return new JsonResponse(array('success' => false));
    }

    public function addCommentAction(Request $request)
    {
        $user = $this->userIsAuthenticated();

        $postid = $request->request->get('postid', 0);
        $comment_text = $request->request->get('comment', "");

        $post = $this->getDoctrine()->getManager()->getRepository('AppBundle:Post')->find($postid);

        if ($post)
        {
            $comment = new Comment();
            $comment->setComment($comment_text);
            $comment->setPostID($post);
            $comment->setUserID($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('New comment on one of your posts')
                ->setFrom('no-reply@9gag-symfony.com')
                ->setTo($comment->getUserID()->getEmail())
                ->setBody($this->renderView(
                    'comment/new_comment_email.html.twig',
                    array('comment' => $comment)
                ),
                    'text/html')
            ;
            $this->get('mailer')->send($message);

            $item = $this->renderView('post/post_item.html.twig', array(
                'post' => $post,
                'detail_item' => 1
            ));

            return new JsonResponse(array('success' => true, 'message' => 'Comment inserted!', 'item' => $item));
        }

        return new JsonResponse(array('success' => false, 'message' => 'An error ocurred!'));
    }

    public function detailPostAction(Request $request, $id)
    {
        $post = $this->getPostObject($id);

        return $this->render('post/post_detail.html.twig', array(
            'post' => $post,
            'post_detail' => 1
        ));
    }

    public function deletePostAction(Request $request)
    {
        $post = $this->userOwnsPost($request->request->get('postid'));
        $query = $this->getDoctrine()->getManager()
            ->createQuery(
                'SELECT c FROM AppBundle:Comment c WHERE c.postID = :postid ORDER BY c.upVotes DESC, c.dateCreated DESC'
            );

        $query->setParameters(array(
            'postid' => $post->getId()
        ));

        $comments = $query->getResult();

        try {

            foreach ($comments as $comment) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($comment);
                $em->flush();
            }

            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();

        } catch (Exception $oEx)
        {
            return new JsonResponse(array('success' => false, 'message' => $oEx->getContent()));
        }

        return new JsonResponse(array('success' => true, 'message' => "Object deleted with success!", 'redirect_url' => $this->generateUrl('homepage')));
    }
}