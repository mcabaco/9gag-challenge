<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Comment")
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $userID;

    /**
     * @ORM\ManyToOne(targetEntity="Post")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     */
    protected $postID;

    /**
     * @ORM\ManyToOne(targetEntity="Comment")
     * @ORM\JoinColumn(name="comment_parent_id", referencedColumnName="id")
     */
    protected $commentParentID;

    /**
     * @ORM\Column(type="text", name="comment")
     */
    protected  $comment;

    /**
     * @ORM\Column(type="integer", name="upVotes")
     */
    protected $upVotes = 0;

    /**
     * @ORM\Column(type="integer", name="downVotes")
     */
    protected $downVotes = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAltered;

    public function __construct()
    {
        $this->dateCreated = new \DateTime("now");
        $this->dateAltered = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set upVotes
     *
     * @param integer $upVotes
     *
     * @return Comment
     */
    public function setUpVotes($upVotes)
    {
        $this->upVotes = $upVotes;

        return $this;
    }

    /**
     * Get upVotes
     *
     * @return integer
     */
    public function getUpVotes()
    {
        return $this->upVotes;
    }

    /**
     * Set downVotes
     *
     * @param integer $downVotes
     *
     * @return Comment
     */
    public function setDownVotes($downVotes)
    {
        $this->downVotes = $downVotes;

        return $this;
    }

    /**
     * Get downVotes
     *
     * @return integer
     */
    public function getDownVotes()
    {
        return $this->downVotes;
    }

    /**
     * Set userID
     *
     * @param \AppBundle\Entity\User $userID
     *
     * @return Comment
     */
    public function setUserID(\AppBundle\Entity\User $userID = null)
    {
        $this->userID = $userID;

        return $this;
    }

    /**
     * Get userID
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * Set postID
     *
     * @param \AppBundle\Entity\Post $postID
     *
     * @return Comment
     */
    public function setPostID(\AppBundle\Entity\Post $postID = null)
    {
        $this->postID = $postID;

        return $this;
    }

    /**
     * Get postID
     *
     * @return \AppBundle\Entity\Post
     */
    public function getPostID()
    {
        return $this->postID;
    }

    /**
     * Set commentParentID
     *
     * @param \AppBundle\Entity\Comment $commentParentID
     *
     * @return Comment
     */
    public function setCommentParentID(\AppBundle\Entity\Comment $commentParentID = null)
    {
        $this->commentParentID = $commentParentID;

        return $this;
    }

    /**
     * Get commentParentID
     *
     * @return \AppBundle\Entity\Comment
     */
    public function getCommentParentID()
    {
        return $this->commentParentID;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Comment
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateAltered
     *
     * @param \DateTime $dateAltered
     *
     * @return Comment
     */
    public function setDateAltered($dateAltered)
    {
        $this->dateAltered = $dateAltered;

        return $this;
    }

    /**
     * Get dateAltered
     *
     * @return \DateTime
     */
    public function getDateAltered()
    {
        return $this->dateAltered;
    }
}
