/**
 * Created by Miguel on 25/11/2015.
 */
var Comment =
{
    Vote: function (oCaller, sUrl, commentid, voteType)
    {
        var postData = $.param({'commentid': commentid, 'voteType' : voteType});

        $.ajax({
            url: sUrl,
            type: 'POST',
            dataType: 'json',
            data: postData,
            success: function(response)
            {
                $(oCaller).parents('.comment').replaceWith(response.item);
            },
            error: function(response)
            {
                alert(response.message);
            }
        });
    },

    Edit: function(oCaller)
    {
        var entryElem = $(oCaller).parents('.comment').children('.comment-entry');
        $(entryElem).children('.comment-text').hide();
        $(entryElem).children('.comment-edit').show();
    },

    Delete: function(oCaller, sUrl, commentID)
    {
        var postData = $.param({'commentid': commentID});

        $.ajax({
            url: sUrl,
            type: 'POST',
            dataType: 'json',
            data: postData,
            success: function(response)
            {
                alert(response.message);
                $(oCaller).parents('.comment').remove();

            },
            error: function(response)
            {
                alert(response.message);
            }
        });
    },

    CancelEdit: function(oCaller)
    {
        var entryElem = $(oCaller).parents('.comment').children('.comment-entry');
        $(entryElem).children('.comment-text').show();
        $(entryElem).children('.comment-edit').hide();

        $(entryElem).children('.comment-edit').children('#new-comment-text').val($(entryElem).children('.comment-text').html());
    },

    ConfirmEdit: function(oCaller, sUrl, commentID)
    {
        var entryElem = $(oCaller).parents('.comment').children('.comment-entry');
        var comment = $(entryElem).children('.comment-edit').children('#new-comment-text').val().trim();

        if (comment.length >= 10 && comment.length <= 100)
        {
            var postData = $.param({'commentid': commentID, 'commentText' : comment});

            $.ajax({
                url: sUrl,
                type: 'POST',
                dataType: 'json',
                data: postData,
                success: function(response)
                {
                    if(response.success)
                    {
                        $(oCaller).parents('.comment').replaceWith(response.item);
                    }
                    else
                    {
                        alert(success.message);
                    }
                },
                error: function(response)
                {
                    alert(response.message);
                }
            });
        }
        else {
            alert('Your post title has to be between 10 and 100 characters.');
        }
    },

    OpenConfirmationDialog: function(funcToCall)
    {
        $('#dialog-confirm').dialog({
            resizable: false,
            autoOpen: false,
            modal: true,
            title: 'Insert Comment',
            height: 200,
            width: 400,
            buttons: {
                "YES": function (){
                    $(this).dialog('close');
                    funcToCall();
                },
                "NO": function (){
                    $(this).dialog('close');
                }

            }
        });

        $('#dialog-confirm').dialog("open");
    }
};
