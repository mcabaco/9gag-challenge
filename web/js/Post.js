var Post = {
    addComment: function (oCaller, sUrl) {
        var formData = $(oCaller).closest('#addCommentForm').serialize();

        $.ajax({
            url: sUrl,
            type: 'POST',
            dataType: 'json',
            data: formData,
            success: function (response) {
                $(oCaller).parents('.post').replaceWith(response.item);
                alert(response.message);
            },
            error: function(response)
            {
                alert(response.message);
            }
        });
    },

    Vote: function (oCaller, sUrl, postid, voteType, detail_item) {
        var postData = $.param({'postid': postid, 'voteType': voteType, 'detail_item': detail_item});

        $.ajax({
            url: sUrl,
            type: 'POST',
            dataType: 'json',
            data: postData,
            success: function (response) {
                $(oCaller).closest('.post').replaceWith(response.item);
            },
            error: function (errorMessage) {
                alert(errorMessage);
            }
        });
    },

    Delete: function (oCaller, sUrl, id) {
        var postData = $.param({'postid': id});

        $.ajax({
            url: sUrl,
            type: 'POST',
            dataType: 'json',
            data: postData,
            success: function (response) {
                alert(response.message);
                if (response) {
                    window.location.href = response.redirect_url;
                }
            },
            error: function (response) {
                alert(response.message);
            }
        });
    },

    ValidateInput: function(oCaller)
    {
        var text = oCaller.value.trim();
        if (text.length >= 10 && text.length <= 100)
        {
            $('#label_warning').hide();
            $('#form_save').attr('disabled', false);
        }
        else
        {
            $('#label_warning').show();
            $('#form_save').attr('disabled', true);
        }
    },

    OpenConfirmationDialog: function(funcToCall)
    {
        $('#dialog-confirm').dialog({
            resizable: false,
            autoOpen: false,
            modal: true,
            title: 'Insert Comment',
            height: 200,
            width: 400,
            buttons: {
                "YES": function (){
                    $(this).dialog('close');
                    funcToCall();
                },
                "NO": function (){
                    $(this).dialog('close');
                }

            }
        });

        $('#dialog-confirm').dialog("open");
    }
};
